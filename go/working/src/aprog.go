/**
 * 'aprog.go'
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 08-22-2019--15:19:58
 *
**/

package main

import (
	"fmt"
	"os"
)

func main() {
	inputFile := os.Args[1]
	outputFile := os.Args[2]
	logFile := os.Args[3]

	fmt.Println(inputFile)
	fmt.Println(outputFile)
	fmt.Println(logFile)
}

/**
 * End 'aprog.go'
**/
